package com.vitaminfmproject.util

import android.os.Build



class Utils {

    companion object {

       public fun getDefaultUserAgent(): String {
            val result = StringBuilder(64)
            result.append("Dalvik/")
            result.append(System.getProperty("java.vm.version")) // such as 1.1.0
            result.append(" (Linux; U; Android ")

            val version = Build.VERSION.RELEASE // "1.0" or "3.4b5"
            result.append(if (version.length > 0) version else "1.0")

            // add the model for the release build
            if ("REL" == Build.VERSION.CODENAME) {
                val model = Build.MODEL
                if (model.length > 0) {
                    result.append("; ")
                    result.append(model)
                }
            }
            val id = Build.ID // "MASTER" or "M4-rc20"
            if (id.length > 0) {
                result.append(" Build/")
                result.append(id)
            }
            result.append(")")
            return result.toString()
        }
    }
}