package com.vitaminfmproject.networking

import com.vitaminfmproject.model.MusicDetailModel
import com.vitaminfmproject.model.RadioStreamModel
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ServiceApiClient {

    @GET("yayin_akisi.json")
    fun getRadioStream(): Observable<List<RadioStreamModel>>

    @GET("calansarki.json")
    fun getMusicDetail(): Observable<MusicDetailModel>

    @GET("posts/{id}")
    fun getUser(@Path("id") id: Int): Observable<RadioStreamModel>

    @Headers("Content-Type: application/json;charset=utf-8")
    @POST("posts")
    fun addUser(@Body article: RadioStreamModel): Observable<RadioStreamModel>

    companion object {

        fun create(): ServiceApiClient {

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("http://vitaminfm.com/cagatayapi/")
                    .build()

            return retrofit.create(ServiceApiClient::class.java)

        }
    }
}