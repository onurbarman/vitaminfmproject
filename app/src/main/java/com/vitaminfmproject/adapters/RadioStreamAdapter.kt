package com.vitaminfmproject.adapters


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.vitaminfmproject.R
import com.vitaminfmproject.model.RadioStreamModel


class RadioStreamAdapter(val streamList: List<RadioStreamModel>): RecyclerView.Adapter<RadioStreamAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder?.txtID?.text = streamList[position].baslangicSaati
        holder?.txtTitle?.text = streamList[position].baslangicSaati
        holder?.txtBody?.text = streamList[position].baslangicSaati
    }

    override fun onCreateViewHolder(parentView: ViewGroup, p1: Int): ViewHolder {
        val v = LayoutInflater.from(parentView?.context).inflate(R.layout.cardview_radio_stream, parentView, false)
        return ViewHolder(v);
    }

    override fun getItemCount(): Int {
        return streamList.size
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val txtTitle = itemView.findViewById<TextView>(R.id.txt_title)
        val txtBody = itemView.findViewById<TextView>(R.id.txt_body)
        val txtID = itemView.findViewById<TextView>(R.id.txt_user_id)
    }

}