package com.vitaminfmproject.activities


import android.content.Context
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.upstream.*
import com.google.android.exoplayer2.util.Util
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.HttpDataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.vitaminfmproject.R
import com.vitaminfmproject.di.Constants


class RadioPlayerActivity : AppCompatActivity() {

    private lateinit var player: SimpleExoPlayer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_radio_player)

        val playButton :Button=  findViewById(R.id.btn_play)
        val pauseButton:Button = findViewById(R.id.btn_stop)

        initPlayer()

        playButton.setOnClickListener{
            player.playWhenReady = true
        }

        pauseButton.setOnClickListener{
            player.playWhenReady = false
        }
    }


    private fun buildHttpDataSourceFactory(bandwidthMeter: DefaultBandwidthMeter): HttpDataSource.Factory {
        return DefaultHttpDataSourceFactory(
                Util.getUserAgent(applicationContext,"ExoPlayerDemo"),
                bandwidthMeter /* listener */,
                DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS,
                DefaultHttpDataSource.DEFAULT_READ_TIMEOUT_MILLIS,
                true /* allowCrossProtocolRedirects */
        )
    }


    private fun buildDataSourceFactory(ctx: Context, bandwidthMeter: DefaultBandwidthMeter): DataSource.Factory {
        return DefaultDataSourceFactory(ctx, bandwidthMeter,
                buildHttpDataSourceFactory(bandwidthMeter))
    }

    fun initPlayer()
    {
        val bandwidthMeter = DefaultBandwidthMeter()
        val extractorsFactory = DefaultExtractorsFactory()
        val trackSelectionFactory = AdaptiveTrackSelection.Factory(bandwidthMeter)
        val trackSelector = DefaultTrackSelector(trackSelectionFactory)
        val defaultBandwidthMeter = DefaultBandwidthMeter()

        val userAgent = Util.getUserAgent(this, "ExoPlayerDemo")


        val dataSourceFactory = DefaultDataSourceFactory(this, userAgent,
                defaultBandwidthMeter)


       val   dataSourceFactory2 = buildDataSourceFactory(applicationContext,bandwidthMeter)

        /*
        val   DEFAULT_COOKIE_MANAGER:CookieManager= CookieManager()
        DEFAULT_COOKIE_MANAGER.setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER);
        if (CookieHandler.getDefault() != DEFAULT_COOKIE_MANAGER)
        {
            CookieHandler.setDefault(DEFAULT_COOKIE_MANAGER);
        }
        */


        buildHttpDataSourceFactory(bandwidthMeter)

        val mediaSource = ExtractorMediaSource(Uri.parse(Constants.RADIO_STREAM_URL),
                dataSourceFactory2, extractorsFactory, null, null)

        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector)
        player.prepare(mediaSource)
    }


    override fun onDestroy() {
        super.onDestroy()
        player.playWhenReady = false
    }
}