package com.vitaminfmproject.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.vitaminfmproject.R
import com.vitaminfmproject.networking.ServiceApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class CostumerSupportActvity : AppCompatActivity() {


    val serviceClient by lazy {
        ServiceApiClient.create()
    }
    var disposable: Disposable? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_costumer_support_actvity)



    }

    fun getRadioStream() {

        disposable = serviceClient.getRadioStream()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            result -> Log.v("RADIO STREAM", "" + result)
                            //bindToRecycleview(result)

                        },
                        { error -> Log.e("ERROR", error.message) }
                )
    }

}
