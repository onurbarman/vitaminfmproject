package com.vitaminfmproject.model

data class RadioStreamModel(
        val baslangicSaati: String,
        val bitisSaati: String,
        val yayinAdi: String,
        val dj: String
)