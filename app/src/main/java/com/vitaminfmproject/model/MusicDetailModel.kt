package com.vitaminfmproject.model
import com.google.gson.annotations.SerializedName

data class MusicDetailModel(

        @SerializedName("LineCount")
        val lineCount: Int,

        @SerializedName("Lines")
        val lines: List<Line>
)


data class Line(

        /*

        {"LineCount": 1, "Lines": [{"Time": "23:33:15","IdType": 1,"Id": 128706,

        "Artist": "Birgül Dönen",
        "Title": "Oy Dünya",
        "Code": "",
        "Album": " ",
        "Duration": "05:24.1","StationCode": ""}]}
         */

        @SerializedName("Artist")
        val artist: String,

        @SerializedName("Title")
        val title: String,

        @SerializedName("Code")
        val code: String,

        @SerializedName("Album")
        val album: String
)